<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/mobil', 'API\MobilController@index');
Route::get('/mobil/create', 'API\MobilController@create');
Route::post('/mobil', 'API\MobilController@store');
Route::get('/mobil/edit/{mobil}','API\MobilController@edit');
Route::put('/mobil/{mobil}','API\MobilController@update');
Route::get('/mobil/{mobil}','API\MobilController@show');
Route::delete('/mobil/{mobil}','API\MobilController@destroy');