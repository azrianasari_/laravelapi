<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mobil;
use App\Pemilik;
class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //ambil semua data dari tabel mobil di database
        $data = Mobil::all();

        //tampilkan dan arahkan ke halaman index
        return view('mobil.index', compact ('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //tampilkan data pemilik
        $pemilik = Pemilik::all();

        return view('mobil.create', compact('pemilik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //cek data foto
         $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);
    
        //save data ke database, 'Mobil' adalah nama model
        Mobil::create([
            'kode' => $request->kode,
            'tahun' => $request->tahun,
            'warna' => $request->warna,
            'no_plat' => $request->no_plat,
            'no_mesin' => $request->no_mesin,
            'no_rangka' => $request->no_rangka,
            'status_mobil' => $request->status_mobil,
            'merk' => $request->merk,
            'tipe' => $request->tipe,
            'pemilik_id' => $request->pemilik_id,
            'foto' => $this->saveImage($request->file('foto'))
        ]);

        //arahkan ke halaman mobil dan tampilkan pesan/status
        return redirect('/mobil')->with('status', 'Data Berhasil Ditambah');
    }

    //fungsi untuk menyimpan data foto
    public function saveImage($file)
    {
       $name = \Carbon\Carbon::now()->format('Y-m-dH:i:s').'-' .$file->getClientOriginalName(). '.' . $file->getClientOriginalExtension();
     
       $path = $file->storeAs('/public/images', $name);
       return $name; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //tampilkan data berdasarkan $id data
         $data = Mobil::findOrFail($id);

         //arahkan dan tampilkan data pada halaman edit
         return view('mobil.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //tampilkan data pemilik
        $pemilik = Pemilik::all();
        
        //tampilkan data berdasarkan $id data
        $data = Mobil::findOrFail($id);

        //arahkan dan tampilkan data pada halaman edit
        return view('mobil.edit', compact('data','pemilik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //cek data foto
         $request->validate([
            'foto' => 'image|mimes:jpg,png,jpeg|max:2048',
        ]);

        $mobil = Mobil::findOrFail($id);
            
        //jika foto kosong maka foto yang di save adalah foto lama
        if($request->file('foto') == null) {
           $request->foto = $mobil->foto;
        }
        else{
           $request->foto = $this->saveImage($request->file('foto'));
        }
       
        Mobil::where('id', $id)
                ->update([
                    'kode' => $request->kode,
                    'tahun' => $request->tahun,
                    'warna' => $request->warna,
                    'no_plat' => $request->no_plat,
                    'no_mesin' => $request->no_mesin,
                    'no_rangka' => $request->no_rangka,
                    'status_mobil' => $request->status_mobil,
                    'merk' => $request->merk,
                    'tipe' => $request->tipe,
                    'pemilik_id' => $request->pemilik_id,
                    'foto' => $request->foto
                ]);

        return redirect('/mobil')->with('status', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         //hapus data berdasarkan $id
         Mobil::destroy($id);

         //arahkan ke halaman mobil dan tampilkan pesan/status
         return redirect('/mobil')->with('status', 'Data Berhasil Dihapus');
    }
}
