<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mobil;
use App\Pemilik;

class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobil = Mobil::all();
        return response()->json([
            'success' => true,
            'message' => 'List of All Mobil',
            'data' => $mobil
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pemilik = Pemilik::all();
        return response()->json([
            'success' => true,
            'message' => 'List of All Pemilik',
            'data' => $pemilik
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mobil = Mobil::create([
            'kode' => $request->kode,
            'tahun' => $request->tahun,
            'warna' => $request->warna,
            'no_plat' => $request->no_plat,
            'no_mesin' => $request->no_mesin,
            'no_rangka' => $request->no_rangka,
            'status_mobil' => $request->status_mobil,
            'merk' => $request->merk,
            'tipe' => $request->tipe,
            'pemilik_id' => $request->pemilik_id,
            'foto' => $this->saveImage($request->file('foto'))
        ]);
        if ($mobil) {
            return response()->json([
                'success' => true,
                'message' => 'Mobil Saved Saved! ',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Mobil Failed to Save!',
            ], 400);
        }
    }

    public function saveImage($file)
    {
       $name = \Carbon\Carbon::now()->format('Y-m-dH:i:s').'-' .$file->getClientOriginalName(). '.' . $file->getClientOriginalExtension();
     
       $path = $file->storeAs('/public/images', $name);
       return $name; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mobil = Mobil::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Mobil',
            'data' => $mobil
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = Mobil::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'List of All Mobil',
            'data' => $mobil
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Mobil::findOrFail($id);
         
        //jika foto kosong maka foto yang di save adalah foto lama
        if($request->file('foto') == null) {
           $request->foto = $data->foto;
        }
        else{
           $request->foto = $this->saveImage($request->file('foto'));
        }
       
        $mobil = Mobil::where('id', $id)
                    ->update([
                    'kode' => $request->kode,
                    'tahun' => $request->tahun,
                    'warna' => $request->warna,
                    'no_plat' => $request->no_plat,
                    'no_mesin' => $request->no_mesin,
                    'no_rangka' => $request->no_rangka,
                    'status_mobil' => $request->status_mobil,
                    'merk' => $request->merk,
                    'tipe' => $request->tipe,
                    'pemilik_id' => $request->pemilik_id,
                    'foto' => $request->foto
                ]);
        
        if ($mobil) {
            return response()->json([
                'success' => true,
                'message' => 'Mobil Updated successfully!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Mobil Failed to Update!',
            ], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = Mobil::findOrFail($id);
        $mobil->delete();
        if ($mobil) {
            return response()->json([
                'success' => true,
                'message' => 'Mobil Delete successfully!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Mobil Failed to Update!',
            ], 500);
        }
    }
}
