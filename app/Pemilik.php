<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
    //nama tabel
    protected $table = "pemiliks";

    //nama kolom
    protected $fillable = ['nama', 'alamat', 'kelurahan', 'kecamatan', 'kab_kota', 'kode', 'kode_pos', 'telp'];

    //relasi ke tabel mobil, 1 pemilik dapat memiliki banyak mobil
    public function mobils(){
    	return $this->hasMany('App\Mobil','pemilik_id');
    }
    
}
