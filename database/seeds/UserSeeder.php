<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name'  => 'admin','email' => 'admin@mail.com','password'  => bcrypt('123123'),'jabatan'  => 0],
            ['name'  => 'kasir','email' => 'kasir@mail.com','password'  => bcrypt('123123'),'jabatan'  => 1]
        ]);
    }
}
