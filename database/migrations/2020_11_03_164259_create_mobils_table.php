<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobils', function (Blueprint $table) {
            $table->id();
            $table->string('kode')->unique();
            $table->string('tahun');
            $table->string('warna');
            $table->string('no_plat');
            $table->string('no_mesin');
            $table->string('no_rangka');
            $table->string('status_mobil');
            $table->string('merk');
            $table->string('tipe');
            $table->text('foto');
            $table->timestamps();
            $table->foreignId('pemilik_id')->constrained('pemiliks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobils');
    }
}
