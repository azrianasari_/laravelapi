@extends('layouts.app')

{{-- isi @yield('content') --}}
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Data</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-body">
                            <form method="POST" action="{{ "/mobil/$data->id" }}"  enctype="multipart/form-data">
                                @method('put')
                                @csrf
                                
                                <div class="form-group row">
                                    <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode</label>
        
                                    <div class="col-md-6">
                                        <input id="kode" type="text" class="form-control @error('kode') is-invalid @enderror" name="kode" value="{{ $data->kode }}" required autocomplete="kode" autofocus>
        
                                        @error('kode')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
    
                                <div class="form-group row">
                                    <label for="tahun" class="col-md-3 offset-md-1 col-form-label text-md-left">Tahun</label>
        
                                    <div class="col-md-6">
                                        <input id="tahun" type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{ $data->tahun }}" required autocomplete="tahun" autofocus>
        
                                        @error('tahun')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="warna" class="col-md-3 offset-md-1 col-form-label text-md-left">Warna</label>
        
                                    <div class="col-md-6">
                                        <input id="warna" type="warna" class="form-control @error('warna') is-invalid @enderror" name="warna" value="{{ $data->warna }}" required autocomplete="warna">
        
                                        @error('warna')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_plat" class="col-md-3 offset-md-1 col-form-label text-md-left">No Plat</label>
        
                                    <div class="col-md-6">
                                        <input id="no_plat" type="no_plat" class="form-control @error('no_plat') is-invalid @enderror" name="no_plat" value="{{ $data->no_plat }}" required autocomplete="no_plat">
        
                                        @error('no_plat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_mesin" class="col-md-3 offset-md-1 col-form-label text-md-left">No Mesin</label>
        
                                    <div class="col-md-6">
                                        <input id="no_mesin" type="no_mesin" class="form-control @error('no_mesin') is-invalid @enderror" name="no_mesin" value="{{ $data->no_mesin }}" required autocomplete="no_mesin">
        
                                        @error('no_mesin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="no_rangka" class="col-md-3 offset-md-1 col-form-label text-md-left">No Rangka</label>
        
                                    <div class="col-md-6">
                                        <input id="no_rangka" type="no_rangka" class="form-control @error('no_rangka') is-invalid @enderror" name="no_rangka" value="{{ $data->no_rangka }}" required autocomplete="no_rangka">
        
                                        @error('no_rangka')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status_mobil" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode Pos</label>
        
                                    <div class="col-md-6">
                                        <select name="status_mobil" id="" class="form-control @error('status_mobil') is-invalid @enderror" required >
                                            <option value="0">Tersedia</option>
                                            <option value="1">Tidak Tersedia</option>
                                        </select>
                               
                                        @error('status_mobil')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="merk" class="col-md-3 offset-md-1 col-form-label text-md-left">Merk</label>
        
                                    <div class="col-md-6">
                                        <input id="merk" type="merk" class="form-control @error('merk') is-invalid @enderror" name="merk" value="{{ $data->merk }}" required autocomplete="new-merk">
        
                                        @error('merk')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="tipe" class="col-md-3 offset-md-1 col-form-label text-md-left">Tipe</label>
        
                                    <div class="col-md-6">
                                        <input id="tipe" type="tipe" class="form-control @error('tipe') is-invalid @enderror" name="tipe" value="{{ $data->tipe }}" required autocomplete="new-tipe">
        
                                        @error('tipe')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="foto" class="col-md-3 offset-md-1 col-form-label text-md-left">Foto</label>
        
                                    <div class="col-md-6">
                                        <input id="foto" type="file" class="form-control @error('foto') is-invalid @enderror" name="foto" value="{{ $data->foto }}" autocomplete="new-foto">
        
                                        @error('foto')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="pemilik_id" class="col-md-3 offset-md-1 col-form-label text-md-left">Pemilik</label>
        
                                    <div class="col-md-6">
                                            <select name="pemilik_id" id="pemilik_id" class="form-control">
                                                @foreach ($pemilik as $p)
                                                    <option value="{{$p->id}}" @if($data->pemilik_id == $p->id) selected @endif  > {{ $p->nama }}</option>
                                                @endforeach
                                            </select>
        
                                        @error('pemilik_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                            
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection