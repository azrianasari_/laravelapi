@extends('layouts.app')

{{-- isi @yield('content') --}}
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Data</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-body">
                        <form method="POST" action="{{ "/pemilik/$data->id" }}">
                            @method('put')
                            @csrf
                            
                            <div class="form-group row">
                                <label for="kode" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode</label>
    
                                <div class="col-md-6">
                                    <input id="kode" type="text" class="form-control @error('kode') is-invalid @enderror" name="kode" value="{{ $data->kode }}" required autocomplete="kode" autofocus>
    
                                    @error('kode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    

                            <div class="form-group row">
                                <label for="nama" class="col-md-3 offset-md-1 col-form-label text-md-left">Nama</label>
    
                                <div class="col-md-6">
                                    <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $data->nama }}" required autocomplete="nama" autofocus>
    
                                    @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="alamat" class="col-md-3 offset-md-1 col-form-label text-md-left">Alamat</label>
    
                                <div class="col-md-6">
                                    <input id="alamat" type="alamat" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ $data->alamat }}" required autocomplete="alamat">
    
                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kelurahan" class="col-md-3 offset-md-1 col-form-label text-md-left">kelurahan</label>
    
                                <div class="col-md-6">
                                    <input id="kelurahan" type="kelurahan" class="form-control @error('kelurahan') is-invalid @enderror" name="kelurahan" value="{{ $data->kelurahan }}" required autocomplete="kelurahan">
    
                                    @error('kelurahan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kecamatan" class="col-md-3 offset-md-1 col-form-label text-md-left">kecamatan</label>
    
                                <div class="col-md-6">
                                    <input id="kecamatan" type="kecamatan" class="form-control @error('kecamatan') is-invalid @enderror" name="kecamatan" value="{{ $data->kecamatan }}" required autocomplete="kecamatan">
    
                                    @error('kecamatan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="kab_kota" class="col-md-3 offset-md-1 col-form-label text-md-left">Kabupaten/Kota</label>
    
                                <div class="col-md-6">
                                    <input id="kab_kota" type="kab_kota" class="form-control @error('kab_kota') is-invalid @enderror" name="kab_kota" value="{{ $data->kab_kota }}" required autocomplete="kab_kota">
    
                                    @error('kab_kota')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="kode_pos" class="col-md-3 offset-md-1 col-form-label text-md-left">Kode Pos</label>
    
                                <div class="col-md-6">
                                    <input id="kode_pos" type="kode_pos" class="form-control @error('kode_pos') is-invalid @enderror" name="kode_pos" value="{{ $data->kode_pos }}" required autocomplete="new-kode_pos">
    
                                    @error('kode_pos')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telp" class="col-md-3 offset-md-1 col-form-label text-md-left">Telepon</label>
    
                                <div class="col-md-6">
                                    <input id="telp" type="telp" class="form-control @error('telp') is-invalid @enderror" name="telp" value="{{ $data->telp }}" required autocomplete="new-telp">
    
                                    @error('telp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                        
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Ubah
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection